1. Using compute engine VM of "Ubuntu 18.04.2 LTS (GNU/Linux 4.15.0-1030-gcp x86_64)".
2. Remove the old file through the command: `yes | rm -r erlin_park`
3. Use command: `git clone https://gitlab.com/hmkao1019/erlin_park.git` to git the Study case file: "erlin_park" under the "anuga_core" path.
4. Main program: `runErlin.py`, please use the version of python 2.7.x to run it.
5. The command is `python /home/[yourusername]/anuga_core/erlin_park/runErlin.py` (Execution path is `./home/[yourusername]`).
6. In the tools file, the 'gdal_translate_1_auto.bat' ~ 'gdal_translate_6_auto.bat' are needed to be changed mode by using the following command `sudo chmod -R 777 gdal_translate_1_auto.bat`, and step by step.
7. In OUTPUT file, there are three python codes, including `runKMLtoJS.py`, `runPlotImg.py` and `runStorage.py`, that are the post-processing and run by using  the version of python 3.x.
8. The command is `python3 /home/[yourusername]/anuga_core/erlin_park/OUTPUT/runKMLtoJS.py` (Execution path is `./home/[yourusername]`).
9. Regarding `runStorage.py`, it transfers the simulated results to the google cloud storage. Therefore, if you want to run it successfully, you'll need to get GCP authorization code with the format of json.
  
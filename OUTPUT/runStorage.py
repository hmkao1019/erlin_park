# -*- coding: utf-8 -*-

autorun_path= './anuga_core/erlin_park/OUTPUT/'

path_to_GCP_AUTH_KEY = autorun_path + 'GCP_authorization_code.json' # GCP 授權碼 (自己申請一個喔!!)

from google.cloud import storage
#from google.cloud.vision import *
# Explicitly use service account credentials by specifying the private key file.
storage_client = storage.Client.from_service_account_json(path_to_GCP_AUTH_KEY) # 取得認證

def upload_to_bucket(path_to_file, path_to_blob, bucket_name):
    """Upload a file to a bucket."""
    bucket = storage_client.get_bucket(bucket_name) # GCP Storage Bucket Name
    blob = bucket.blob(path_to_blob) # folder + filename that you want to save in GCP Bucket
    blob.upload_from_filename(path_to_file) # filename that you want to upload



bucket_name_on_GCP= 'my-project-1019-218107.appspot.com'

#------------------------------------------------------------------------------
json_file1= autorun_path + 'erlin_3600.js'
json_file2= autorun_path + 'erlin_7200.js'
json_file3= autorun_path + 'erlin_10800.js'
json_file4= autorun_path + 'erlin_14400.js'
json_file5= autorun_path + 'erlin_18000.js'
json_file6= autorun_path + 'erlin_21600.js'

json_file_1= 'erlin_3600.js'
json_file_2= 'erlin_7200.js'
json_file_3= 'erlin_10800.js'
json_file_4= 'erlin_14400.js'
json_file_5= 'erlin_18000.js'
json_file_6= 'erlin_21600.js'

upload_to_bucket(json_file1, 'simulation/' + json_file_1, bucket_name_on_GCP)
upload_to_bucket(json_file2, 'simulation/' + json_file_2, bucket_name_on_GCP)
upload_to_bucket(json_file3, 'simulation/' + json_file_3, bucket_name_on_GCP)
upload_to_bucket(json_file4, 'simulation/' + json_file_4, bucket_name_on_GCP)
upload_to_bucket(json_file5, 'simulation/' + json_file_5, bucket_name_on_GCP)
upload_to_bucket(json_file6, 'simulation/' + json_file_6, bucket_name_on_GCP)

#------------------------------------------------------------------------------
png_file1= autorun_path + 'erlin_output_depth_1.png'
png_file2= autorun_path + 'erlin_output_depth_2.png'
png_file3= autorun_path + 'erlin_output_depth_3.png'
png_file4= autorun_path + 'erlin_output_depth_4.png'
png_file5= autorun_path + 'erlin_output_depth_5.png'
png_file6= autorun_path + 'erlin_output_depth_6.png'

png_file_1= 'erlin_output_depth_1.png'
png_file_2= 'erlin_output_depth_2.png'
png_file_3= 'erlin_output_depth_3.png'
png_file_4= 'erlin_output_depth_4.png'
png_file_5= 'erlin_output_depth_5.png'
png_file_6= 'erlin_output_depth_6.png'

upload_to_bucket(png_file1, 'figure/' + png_file_1, bucket_name_on_GCP)
upload_to_bucket(png_file2, 'figure/' + png_file_2, bucket_name_on_GCP)
upload_to_bucket(png_file3, 'figure/' + png_file_3, bucket_name_on_GCP)
upload_to_bucket(png_file4, 'figure/' + png_file_4, bucket_name_on_GCP)
upload_to_bucket(png_file5, 'figure/' + png_file_5, bucket_name_on_GCP)
upload_to_bucket(png_file6, 'figure/' + png_file_6, bucket_name_on_GCP)

#------------------------------------------------------------------------------
movie_file1= autorun_path + 'erlin_6h_movie.gif'
movie_file_1= 'erlin_6h_movie.gif'
upload_to_bucket(movie_file1, 'figure/' + movie_file_1, bucket_name_on_GCP)
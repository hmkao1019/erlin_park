# -*- coding: utf-8 -*-

import kml2geojson

# use Python3

autorun_path= './anuga_core/erlin_park/OUTPUT/'

def KMLtoGeoJson(): 

    kml2geojson.main.convert(autorun_path + 'erlin_3600.kml' , autorun_path)
    kml2geojson.main.convert(autorun_path + 'erlin_7200.kml' , autorun_path)
    kml2geojson.main.convert(autorun_path + 'erlin_10800.kml', autorun_path)
    kml2geojson.main.convert(autorun_path + 'erlin_14400.kml', autorun_path)
    kml2geojson.main.convert(autorun_path + 'erlin_18000.kml', autorun_path)
    kml2geojson.main.convert(autorun_path + 'erlin_21600.kml', autorun_path)

KMLtoGeoJson()


def JSONtoJS(geojson_file, js_file):
    with open (geojson_file, 'r') as gs:
        gs_read= gs.read()
    with open(js_file, 'w') as js_write:
        #js_write.write(gs_read)
        js_write.write('eqfeed_callback('+ gs_read + ')')
        

JSONtoJS(autorun_path + 'erlin_3600.geojson' , autorun_path + 'erlin_3600.js' )
JSONtoJS(autorun_path + 'erlin_7200.geojson' , autorun_path + 'erlin_7200.js' )
JSONtoJS(autorun_path + 'erlin_10800.geojson', autorun_path + 'erlin_10800.js')
JSONtoJS(autorun_path + 'erlin_14400.geojson', autorun_path + 'erlin_14400.js')
JSONtoJS(autorun_path + 'erlin_18000.geojson', autorun_path + 'erlin_18000.js')
JSONtoJS(autorun_path + 'erlin_21600.geojson', autorun_path + 'erlin_21600.js')






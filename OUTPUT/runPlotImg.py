import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import requests
import os
import imageio



def custom_div_cmap(numcolors=20, name='custom_div_cmap', mincol='lightskyblue', midcol='blue', maxcol='red'):
    from matplotlib.colors import LinearSegmentedColormap
    cmap = LinearSegmentedColormap.from_list(name = name, colors=[mincol, midcol, maxcol], N=numcolors)
    cmap.set_under('white', alpha=0)
    cmap.set_over('red')
    return cmap

autorun_path= './anuga_core/erlin_park/'
path_to_image = autorun_path + 'GeoSource/' + 'ERLIN.png'


# Calculate time----------------
RL_url_R= "http://ctsp-sws.dps.tw:4713/RainData/RL/RainData.txt"
Rain = requests.get(RL_url_R)
datalist_R= Rain.text.split()
#-------------------------------
ticks= 0
for i in range (6):    
    ticks= ticks + 3600
    name= "erlin_output_depth_" + str(ticks)
    FileName= autorun_path + 'OUTPUT/' + name + ".asc"
    png_name= "erlin_output_depth_" + str(i+1) 

    time= datalist_R[169 + 7*i]+'-'+datalist_R[170 + 7*i ]+'-'+datalist_R[171 + 7*i]+' '+datalist_R[172 + 7*i ]+':00:00'
    figure_title = "Forecast at time  " + str(time)
    
    
    # Add background image file
    fig, ax = plt.subplots()
    img = mpimg.imread(path_to_image)
    plt.imshow(img, zorder=0, extent=[189441.803399991, 194920.266782731, 2645522.62436021, 2649340.0093915])


    with open(FileName, 'r') as prism_f:
        prism_header = prism_f.readlines()[:6]
        prism_header = [item.strip().split()[-1] for item in prism_header]
        prism_cols = int(prism_header[0])
        prism_rows = int(prism_header[1])
        prism_xll = float(prism_header[2])
        prism_yll = float(prism_header[3])
        prism_cs = float(prism_header[4])
        prism_nodata = float(prism_header[5])
        prism_array = np.loadtxt(FileName, dtype=np.float, skiprows=6)
        # Set the nodata values to nan
        prism_array[prism_array == prism_nodata] = np.nan
        cmap = custom_div_cmap()
        # Extent is Xmin, Xmax, Ymin, Ymax
        prism_extent = [
                prism_xll, prism_xll + prism_cols * prism_cs,
                prism_yll, prism_yll + prism_rows * prism_cs]
        # Get the img object in order to pass it to the colorbar function
        img_plot = ax.imshow(prism_array, cmap=cmap, extent=prism_extent, interpolation=None, vmin=0.5, vmax=2,
                             alpha=.8)
        plt.colorbar(img_plot, cmap=cmap, label='Water Depth (m)')
        #ax.grid(False)
        plt.axis('off')
        plt.title(figure_title)
       

    filename = autorun_path + 'OUTPUT/' + png_name + '.png'
    plt.savefig(filename)


plt.close()


# png to gif
"""
png_dir = autorun_path + 'OUTPUT/'
images = []
for file_name in os.listdir(png_dir):
    if file_name.endswith('.png'):
        file_path = os.path.join(png_dir, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave( autorun_path + 'OUTPUT/erlin_6h_movie.gif', images,  duration = 1.0)
"""

png_dir = autorun_path + 'OUTPUT/'
images = []
images.append(imageio.imread(png_dir + 'erlin_output_depth_1.png'))
images.append(imageio.imread(png_dir + 'erlin_output_depth_2.png'))
images.append(imageio.imread(png_dir + 'erlin_output_depth_3.png'))
images.append(imageio.imread(png_dir + 'erlin_output_depth_4.png'))
images.append(imageio.imread(png_dir + 'erlin_output_depth_5.png'))
images.append(imageio.imread(png_dir + 'erlin_output_depth_6.png'))

imageio.mimsave( autorun_path + 'OUTPUT/' + 'erlin_6h_movie.gif', images,  duration = 1.0)





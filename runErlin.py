# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 19:38:28 2019
@author: HMKao
"""

#------------------------------------------------------------------------------
# Import necessary modules
#------------------------------------------------------------------------------
# Standard modules
import os
import time
import sys
import requests

# Related major packages
import anuga            

# Local modules
from anuga import Rate_operator
from tools.read_boundary_tags_line_shapefile import read_boundary_tags_line_shapefile
from tools.export_results import sww2asc, MakeGeoTif
from tools.batchRun import runbatch1, runbatch2, runbatch3, runbatch4, runbatch5, runbatch6 
from tools.TWD97toWGS84 import TWD97toWGS84
from tools.XYZtoKML import XYZtoKML

use_cache = False 
verbose = True

# Due to autorun with crontab set in GCP
autorun_path= './anuga_core/erlin_park/'
#autorun_path= ''

# infiltration with constnat rate 
def setup_infiltration_with_ConstantRate(domain):
    rate = -40.0  #unit: mm/h
    print (str(rate) + ' mm/h')
    
    return Rate_operator(domain, rate=rate, factor=1.0e-3/3600)  #Unit: mm/h-->m/sec


def setup_rainfall_with_ConstantRate(domain):
    rate = 0  #unit: mm/h
    print (str(rate) + ' mm/h')
    
    return Rate_operator(domain, rate=rate, factor=1.0e-3/3600)  #Unit: mm/h-->m/sec

#------------------------------------------------------------------------------
def Rain_get_url(Rain_url):
    global datalist1_R
    Rain = requests.get(Rain_url)
    datalist_R= Rain.text.split()
    datalist1_R = [float(datalist_R[i+6]) for i in range(0,len(datalist_R),7)]
    
    for i in range(0,96):
        # Sanity check 'Negative rainfall input'
        if datalist1_R[i]< 0:
            datalist1_R[i]= 0.0
        
    return datalist1_R


def setup_rainfall_url(domain,t):
    RL_url_R= "http://ctsp-sws.dps.tw:4713/RainData/RL/RainData.txt"
    Rain_get_url(RL_url_R)
        
    if t<=3600: 
        rate= datalist1_R[24]  #預報第1小時
    elif t>3600 and t <=7200:
        rate= datalist1_R[25]
    elif t>7200 and t <=10800:
        rate= datalist1_R[26]
    elif t>10800 and t <=14400:
        rate= datalist1_R[27]
    elif t>14400 and t <=18000:
        rate= datalist1_R[28]
    elif t>18000 and t <=21600:
        rate= datalist1_R[29]
        
    print (str(rate) + ' mm/h')
        
    return Rate_operator(domain, rate=rate, factor=1.0e-3/3600) #factor means Unit: mm/h-->m/sec

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def Rain_get():
    global datalist
    datalist= []
    #with open('./Rainfall/RainData.txt', 'r') as f:
    with open(autorun_path + 'Rainfall/RainData.txt', 'r') as f:
        for i in range(96):
            data= f.readline()
            datalist.append(data.split())
    return datalist


def setup_rainfall(domain,t):
    Rain_get()
    for i in range(6):
        i=i+24
        # Sanity check 'Negative rainfall input'
        if(float(datalist[i][6])<0):
            datalist[i][6]= 0.0
        
    if t<=3600:  #Unit: sec.
        rate= float(datalist[24][6])
    elif t>3600 and t <=7200:
        rate= float(datalist[25][6])
    elif t>7200 and t <=10800:
        rate= float(datalist[26][6])
    elif t>10800 and t <=14400:
        rate= float(datalist[27][6])
    elif t>14400 and t <=18000:
        rate= float(datalist[28][6])
    elif t>18000 and t <=21600:
        rate= float(datalist[29][6])
        
    print (str(rate) + ' mm/h')
            
    return Rate_operator(domain, rate=rate, factor=1.0e-3/3600)

#------------------------------------------------------------------------------

StudyDomain = 'erlin'
t0 = time.time()

#------------------------------------------------------------------------------
# Preparation of topographic data
# Convert ASC 2 DEM 2 PTS using source data and store result in source data
#------------------------------------------------------------------------------


# Create DEM from asc data
anuga.asc2dem(autorun_path + 'GeoSource/' + StudyDomain +'.asc', use_cache = use_cache, verbose = verbose)

# Create pts file for onshore DEM
anuga.dem2pts(autorun_path + 'GeoSource/' + StudyDomain + '.dem', use_cache = use_cache, verbose = verbose)

# Read bounding_polygon and boundary_tags
bounding_polygon, boundary_tags= read_boundary_tags_line_shapefile(autorun_path + 'GeoSource/' + StudyDomain + 'extent.shp')

A = anuga.polygon_area(bounding_polygon) / 1000000.0  # m^2 --> km^2
print('Area of bounding polygon = %.2f km^2' % A)

#------------------------------------------------------------------------------
# Create the triangular mesh and domain based on 
# overall clipping polygon with a tagged boundary and/or interior regions
#------------------------------------------------------------------------------

base_scale = 400  # Background resolution;  Maximum area of triangle mesh 
default_res = 25 * base_scale 


# Read interior polygons
poly_ErlinCenter = anuga.read_polygon(autorun_path + 'GeoSource/' + 'ErlinCenter.csv')
# Define list of interior regions with associated resolutions
interior_regions = [[poly_ErlinCenter,  base_scale]]

# Create simulated domain
domain = anuga.create_domain_from_regions(bounding_polygon,
                                          boundary_tags,
                                          maximum_triangle_area= default_res,
                                          mesh_filename= autorun_path + 'GeoSource/' + StudyDomain +'.msh',
                                          interior_regions= interior_regions,   # region with finer grid 
                                          use_cache= use_cache,
                                          verbose= verbose)

# Print some stats about mesh and domain
print ( 'Number of triangles = ', len(domain) )
print ( 'The extent is ', domain.get_extent() )
print ( domain.statistics() )

                                    
#------------------------------------------------------------------------------
# Setup parameters of computational domain
#------------------------------------------------------------------------------
domain.set_name(StudyDomain+'_output')        # Name of sww file
#domain.set_datadir('.')                      # Store sww output here
domain.set_datadir(autorun_path + 'OUTPUT/')   # Store sww output in autorun_path
domain.set_minimum_storable_height(0.01)      # Store only depth > 1cm
domain.set_flow_algorithm('DE0')            


#------------------------------------------------------------------------------
# Setup initial conditions
#------------------------------------------------------------------------------
domain.set_quantity('stage', 0.0)
domain.set_quantity('friction', 0.015)  # Manning n value for buildings and roads is set 0.015.
domain.set_quantity('elevation', 
                    filename= autorun_path + 'GeoSource/' + StudyDomain + '.pts',
                    use_cache= use_cache,
                    verbose= verbose,
                    alpha=0.1)


#------------------------------------------------------------------------------
# Setup boundary conditions
#------------------------------------------------------------------------------
print ( 'Available boundary tags', domain.get_boundary_tags() ) 

# Solid reflective wall 
Br= anuga.Reflective_boundary(domain)                                         
# i.e. The stage value at the boundary is 0.2 and the xmomentum and ymomentum at the boundary are set to 0.0. 
Bd= anuga.Dirichlet_boundary([0.2, 0.0, 0.0]) 
# Neutral boundary
Bs= anuga.Transmissive_stage_zero_momentum_boundary(domain) 

domain.set_boundary({'downstream_BC': Bd, 'upstream_BC': Br, 'midstream_BC': Bd})
#domain.set_boundary({'downstream_BC': Bs, 'upstream_BC': Bs, 'midstream_BC': Bs})


#------------------------------------------------------------------------------
# Evolve system through time
#------------------------------------------------------------------------------

from numpy import allclose

# Making rainfal with timeseries & Save every minute
for t in domain.evolve(yieldstep=600, finaltime=21600):  # Unit: sec.
    print 'Making rainfall input is'
    #setup_rainfall(domain,t)
    setup_rainfall_url(domain,t)

    print 'Making infiltration input is'
    setup_infiltration_with_ConstantRate(domain)
 
    print domain.timestepping_statistics()
    domain.set_quantities_to_be_stored({'stage': 2, 'elevation': 1})  #2: save at every timestep; 1:save at the beginning
    

# Post-processing


#sww2asc(num, cellsize, reduction)  
#num--> 0:Stage, 1:Absolute Momentum, 2:Depth, 3:Speed, 4:Elevation
#Output time(sec)= yieldstep*reduction
sww2asc(2, 5, 6)
sww2asc(2, 5, 12)
sww2asc(2, 5, 18)
sww2asc(2, 5, 24)
sww2asc(2, 5, 30)
sww2asc(2, 5, 36)

#MakeGeoTif(timestep, cellsize, bounding_polygon)
#Output time(sec)= yieldstep*timestep
MakeGeoTif(6 , 20, bounding_polygon)
MakeGeoTif(12, 20, bounding_polygon)
MakeGeoTif(18, 20, bounding_polygon)
MakeGeoTif(24, 20, bounding_polygon)
MakeGeoTif(30, 20, bounding_polygon)
MakeGeoTif(36, 20, bounding_polygon)

print('Run batch file')
runbatch1()
runbatch2()
runbatch3()
runbatch4()
runbatch5()
runbatch6()

print('From TWD97 to WGS84')
TWD97toWGS84()

print('From XYZ to KML')
XYZtoKML()


print 'That took %.2f seconds' %(time.time()-t0)




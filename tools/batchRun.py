# -*- coding: utf-8 -*-

import subprocess

autorun_path= './anuga_core/erlin_park/tools/'
#autorun_path= ''

def runbatch1():
    path_to_batch_file = autorun_path + 'gdal_translate_1_auto.bat'
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()
    
def runbatch2():
    path_to_batch_file = autorun_path + 'gdal_translate_2_auto.bat'
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()

def runbatch3():
    path_to_batch_file = autorun_path + 'gdal_translate_3_auto.bat' 
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()

def runbatch4():
    path_to_batch_file = autorun_path + 'gdal_translate_4_auto.bat' 
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()

def runbatch5():
    path_to_batch_file = autorun_path + 'gdal_translate_5_auto.bat'
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()
    
def runbatch6():
    path_to_batch_file = autorun_path + 'gdal_translate_6_auto.bat'
    p = subprocess.Popen(path_to_batch_file, shell=True, stdout=subprocess.PIPE)
    p.wait()
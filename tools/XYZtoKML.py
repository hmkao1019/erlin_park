# -*- coding: utf-8 -*-

import csv
import simplekml


def openfile(csvfile, kmlfile):
    inputfile = csv.reader(open(csvfile,'r'))
    kml=simplekml.Kml()

    for row in inputfile:
        kml.newpoint(name=row[2], coords=[(row[1],row[0])])

    kml.save(kmlfile)    

def XYZtoKML():
    autorun_path= './anuga_core/erlin_park/OUTPUT/'
    openfile(autorun_path + 'lonlat_3600.csv' , autorun_path + 'erlin_3600.kml' )
    openfile(autorun_path + 'lonlat_7200.csv' , autorun_path + 'erlin_7200.kml' )
    openfile(autorun_path + 'lonlat_10800.csv', autorun_path + 'erlin_10800.kml')
    openfile(autorun_path + 'lonlat_14400.csv', autorun_path + 'erlin_14400.kml')
    openfile(autorun_path + 'lonlat_18000.csv', autorun_path + 'erlin_18000.kml')
    openfile(autorun_path + 'lonlat_21600.csv', autorun_path + 'erlin_21600.kml')
    

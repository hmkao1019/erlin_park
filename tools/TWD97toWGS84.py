# -*- coding: utf-8 -*-
import twd97
import csv

def openfile(xyzfile, csvfile):
    with open(xyzfile, 'r') as f:
        data= f.readlines()

    datalist= []    
    with open(xyzfile, 'r') as f1:
        for i in range(len(data)):
            data1= f1.readline()
            datalist.append(data1.split())

    #with open('lonlat.csv', 'w', newline='') as csvfile:
    with open(csvfile, 'wb') as csvfile:
        for i in range(len(data)):
            x= float(datalist[i][0])
            y= float(datalist[i][1])
            z= datalist[i][2]
            if z=='nan': z= 0
        
            x_lat, x_long= twd97.towgs84(x, y)
        
            writer = csv.writer(csvfile)
            if (float(z)>=0.5):
                writer.writerow([x_lat, x_long, z])

def TWD97toWGS84():    
    autorun_path= './anuga_core/erlin_park/OUTPUT/'
    openfile(autorun_path + 'output_3600.xyz' , autorun_path + 'lonlat_3600.csv' )
    openfile(autorun_path + 'output_7200.xyz' , autorun_path + 'lonlat_7200.csv' )
    openfile(autorun_path + 'output_10800.xyz', autorun_path + 'lonlat_10800.csv')
    openfile(autorun_path + 'output_14400.xyz', autorun_path + 'lonlat_14400.csv')
    openfile(autorun_path + 'output_18000.xyz', autorun_path + 'lonlat_18000.csv')
    openfile(autorun_path + 'output_21600.xyz', autorun_path + 'lonlat_21600.csv')
    

"""
from pyproj import Proj, transform
# TWD97 EPSG:3826
# WGS84 EPSG:4326

with open('lonlat.csv', 'w', newline='') as csvfile:
    for i in range(len(data)):
        x1= float(datalist[i][0])
        y1= float(datalist[i][1])
        z1= datalist[i][2]
        if z1=='nan': z1= 0
        
        inProj= Proj(init='epsg:3826')
        outProj= Proj(init='epsg:4326')  

        x2,y2= transform(inProj,outProj,x1,y1)
        
        print(x2,y2,z1)
        #writer = csv.writer(csvfile)
        #writer.writerow([x2, y2, z1])
    
"""      
  



import os
import sys

import anuga

autorun_path= './anuga_core/erlin_park/OUTPUT/'
#autorun_path= ''

def sww2asc(num, cellsize, reduction):
    StudyDomain = 'erlin'
    name = StudyDomain + '_output'
    print 'output dir:', name

    which_var = num

    if which_var == 0:    # Stage
        outname = name + '_stage'
        quantityname = 'stage'

    if which_var == 1:    # Absolute Momentum
        outname = name + '_momentum'
        quantityname = '(xmomentum**2 + ymomentum**2)**0.5'    #Absolute momentum

    if which_var == 2:    # Depth
        outname = name + '_depth_' + str(reduction*600) 
        quantityname = 'stage-elevation'  #Depth

    if which_var == 3:    # Speed
        outname = name + '_speed'
        quantityname = '(xmomentum**2 + ymomentum**2)**0.5/(stage-elevation+1.e-3)'  #Speed

    if which_var == 4:    # Elevation
        outname = name + '_elevation'
        quantityname = 'elevation'  #Elevation

    print 'start sww2dem'

    anuga.sww2dem(autorun_path + name + '.sww',
                  autorun_path + outname +'.asc',
                  quantity= quantityname,
                  cellsize= cellsize,      
                  easting_min= 189489.144714,
                  easting_max= 193732.168274,
                  northing_min= 2645523.482910,
                  northing_max= 2649303.435303,        
                  reduction= reduction,   # Output time(sec)= yieldstep*reduction
                  verbose= True,
                  datum= 'D_GRS_1980',
                  block_size= 100000)

#------------------------------------------------------------------------------

from anuga.utilities.plot_utils import Make_Geotif

def MakeGeoTif(timestep, cellsize, bounding_polygon):
    StudyDomain = 'erlin'
    name = StudyDomain + '_output'
    print 'output dir:', name
    print 'start Make_Geotif'
    
    Make_Geotif(swwFile= autorun_path + name + '.sww', 
                output_quantities=['depth'],
                myTimeStep=[timestep],   #According to yieldstep (if yieldstep= 600s). e.g. [10] means 10*600= 6,000s
                CellSize=cellsize, 
                lower_left=None, upper_right=None,
                EPSG_CODE=3826, #TWD97 / TM2 zone 121
                proj4string=None,
                velocity_extrapolation=True,
                min_allowed_height=1.0e-05,
                output_dir= autorun_path,
                bounding_polygon= bounding_polygon,
                verbose=True)
    
#------------------------------------------------------------------------------




